#include "include/task1_main.h"
#include <iostream>
#include <string>
#include <vector>
#include <ctime>

class Track {
	std::string name;
	std::tm madeData;
	int duration;
public:
	Track(std::string &new_name, int durart, std::tm &date) {
		name = new_name;
		duration = durart;
		madeData = date;
	}
	static std::string getTrackName(Track *track) {
		return track->name;
	}

	static void trackInfo(Track *track) {
		std::string result = "";
		result = "����:" + track->name + "\n������������:" + std::to_string(track->duration) + "\n���� ���������:" + std::asctime(&track->madeData);
		std::cout << result << std::endl;
	}
	
};

class Mp3Player {
	std::vector<Track> trackList;
	bool playFl = false;
	int curMelodi;
	//void print_cur_track();
public:
	bool play() { 
		std::string name;

		if (playFl) return true;
		std::cout << "������� ��� ����� ��� �������" << std::endl;
		std::cin >> name;
		curMelodi = -1;
		for (auto &it : trackList) {
//			std::cout << Track::getTrackName(&it) << std::endl;
			curMelodi++;
			if (name != Track::getTrackName(&it)) continue;
			playFl = true;
			Track::trackInfo(&it);
			return true;
		}
		std::cout << "�� ���������� ��� ������ �����" << std::endl;

		return true;
	}
	bool pause() { 
		if (!playFl) return true;
		std::cout << "����" << Track::getTrackName(&trackList[curMelodi]) << " ��������� �� �����" << std::endl;
		playFl = false;
		return true;
	}
	bool next() {
		int list_size = trackList.size();
		int new_melody;

		if (list_size <= 1) {
			std::cout << "������ ������ ���" << std::endl;
			return true;
		}
		new_melody = 0 + rand() % list_size + 1;
		if (new_melody != curMelodi) curMelodi = new_melody;
		else this->next();
		std::cout << "��������� ���� " << std::endl;
		Track::trackInfo(&trackList[curMelodi]);
		playFl = true;
		return true;
	}
	bool stop() { 
		if (!playFl) return true;
		std::cout << "����"<< Track::getTrackName(&trackList[curMelodi]) <<" ����������" << std::endl;
		playFl = false;
		return true;
	}
	bool exit() { 
		std::cout << "��������� �����" << std::endl;
		//exit(1);
		return true;
	}
	void addTrack(Track &newTrek) {
		trackList.push_back(newTrek);
	}
};

void addTrack(Mp3Player &player){
	std::tm date{};
	std::string name;

	
	
	//std::mktime(&date);
	//std::cout << std::asctime(&date) << std::endl;
	for (int i = 1; i < 6; i++) {
		name = ("track" + std::to_string(i));
		date.tm_year = 1995 + rand() % 200;
		date.tm_mday = 1 + rand() % 27;
		Track track(name, 1 + rand() % 5, date);
		player.addTrack(track);
		std::cout << "� ����� �������� ����: " << name << std::endl;
	}
	std::cout <<"� ������ " << 5 << " ������" << std::endl;
}

void task1() {
	int cmd = 0;
	Mp3Player my_player;
	std::cout << "������� �1 ��������" << std::endl;
	addTrack(my_player);
	while (1) {
		std::cout << "������� ����� ��������:\n1 - play\n2 - pause\n3 - next\n4 - stop\n5 - exit" << std::endl;
		std::cin >> cmd;
		//std::cout << "������� ������� " << cmd << std::endl;
		switch (cmd)
		{
		case 1:
			my_player.play();
			break;
		case 2:
			my_player.pause();
			break;
		case 3:
			my_player.next();
			break;
		case 4:
			my_player.stop();
			break;
		case 5:
			my_player.exit();
			return;
		default:
			break;
		}
		//cmd = 0;
	}
}