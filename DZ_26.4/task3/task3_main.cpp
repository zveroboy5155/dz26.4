#include "include/task3_main.h"
#include <iostream>
#include <string>

#define MAX_X 80
#define MAX_Y 50

class Window {
	int x = 0, y = 0;
	int sizeX = 1, sizeY = 1;

	bool checkSize(int xSize, int ySize) {
		if (xSize > MAX_X || xSize < 0) {
			std::cout << "����� �� ������� �������� �� �, �������� ���������, ��������� ��������." << std::endl;
			return false;
		}

		if (ySize > MAX_Y || ySize < 0) {
			std::cout << "����� �� ������� �������� �� y, �������� ���������,��������� ��������." << std::endl;
			return false;
		}
		return true;
	}
public:
	/*Window(int len_x, int with_y){
		
		if (len_x > MAX_X - 1) size_x = MAX_X - 1;
		if (with_y >)

		if (!size_x) size_x = len_x;

	}*/
	void move(int xShift, int yShift) {
		if (!checkSize(xShift + x + sizeX - 1, yShift + y + sizeY - 1)) return;
		x += xShift;
		y += yShift;
		std::cout << "����� ���������� ����: " << x << " " << y << std::endl;
	}
	void resize(int xShift, int yShift) {
		if (!checkSize(xShift + x - 1, yShift + y - 1)) return;
		sizeX = xShift;
		sizeY = yShift;
		std::cout << "����� ������� ����: " << sizeX << " " << sizeY << std::endl;
	}
	void display() {
		bool winFl = false;

		for (int i = 0; i < MAX_Y; i++) {
			for (int j = 0; j < MAX_X; j++) {
				if (i >= y && i < y + sizeY && j >= x && j < x + sizeX) winFl = true;
				else winFl = false;
				
				std::cout << (int)winFl;
			}
			std::cout << std::endl;
		}
	}
	void close() {
		std::cout << "���� �����������" << std::endl;
	}
};

void task3() {
	std::cout << "������ �� task �3" << std::endl;
	int cmd = 0;
	Window window;
	int xShift, yShift;

	//std::cout << "������ �2 ��������" << std::endl;
	while (1) {
		std::cout << "������� ����� ��������:\n1 - move\n2 - resize\n3 - display\n4 - close" << std::endl;
		std::cin >> cmd;
		switch (cmd)
		{
		case 1:
			std::cout << "������� �������� �������� �� � � �� Y" << std::endl;
			std::cin >> xShift >> yShift;
			window.move(xShift, yShift);
			break;
		case 2:
			std::cout << "������� ����� ������� �� � � �� Y" << std::endl;
			std::cin >> xShift >> yShift;
			window.resize(xShift, yShift);
			break;
		case 3:
			window.display();
			break;
		case 4:
			window.close();
			return;
		default:
			break;
		}
	}
}
