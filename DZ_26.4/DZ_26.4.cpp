﻿// DZ_26.4.cpp: определяет точку входа для приложения.
//

#include "DZ_26.4.h"
//#include <task1_main.h>
#include "task1_main.h"
#include "task2_main.h"
#include "task3_main.h"
#include <iostream>
#include <thread>
#include <chrono>
//using namespace std;

int main()
{
	int cmd = -1;

	setlocale(0, "");

	while (cmd != 0) {
		std::cout << "Введите номер задачи(1 - 3) или 0 для выхода" << std::endl;
		std::cin >> cmd;
		switch (cmd)
		{
		case 1:
			task1();
			break;
		case 2:
			//std::cout << "Задача 2 в разработке" << std::endl;
			task2();
			break;
		case 3:
			task3();
		default:
			break;
		}

	}
	//std::this_threat::sleep_for(1000);
	std::cout << "Завершение работы" << std::endl;
	std::this_thread::sleep_for(std::chrono::seconds(2));
	return 0;
}

