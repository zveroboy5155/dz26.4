#include "include/task2_main.h"
#include <iostream>
#include <string>
#include <vector>

class PhoneNote {
	std::string number;
	std::string name;

public:
	static bool isPhoneNum(std::string &Num, bool errFl = true) {
		if (Num.length() > 12)
			return std::cout << ((errFl) ? "������� ������� �����" : "���������� ����� �� �����") << std::endl, false;
			//return false;
			
		if (Num.length() < 12 - 1)
			return std::cout << ((errFl) ? "������� �������� �����" : "���������� ����� �� �����") << std::endl, false;
	
		for (auto it : Num) {
			if ((it == *Num.begin() && it == '+') || (it >= '0' && it <= '9')) continue;
			return std::cout << ((errFl) ? ((std::string)("����� �������� ������������ ������" + it)) : "���������� ����� �� �����") << std::endl, false;
		}
		return true;
	}
	bool initNote(std::string &newName, std::string &newNum) {
		if (!PhoneNote::isPhoneNum(newNum)) return false;
		this->number = newNum;
		name = newName;
		return true;
	}
	bool getNumByName(std::string &num_buff) {
		
		if (num_buff != name) return false;
		num_buff = number;
		return true;
	}
};

class Phone {
	std::vector <PhoneNote> phoneBook;
	
	std::string getNumber() {
		std::string buff;
		//bool search_fl = false;

		std::cout << "������� ��� ��� ����� ��������" << std::endl;
		std::cin >> buff;
		if (PhoneNote::isPhoneNum(buff, false)) return buff;
		
		for (auto it : phoneBook) {
			if (it.getNumByName(buff)) return buff;
		}
		std::cout << "��� �������� � ������ " << buff << std::endl;
		return " ";
	}
public:
	void addNumber() {
		std::string number;
		std::string name;
		PhoneNote newNote;

		do{
			std::cout << "������� ��� ������ �������� ��� cancel ��� ������ ����������" << std::endl;
			std::cin >> name;
			if (name == "cancel") return;
			std::cout << "������� ����� �������� ������ ��������" << std::endl;
			std::cin >> number;
		} while (!newNote.initNote(name, number));
		
		phoneBook.push_back(newNote);
	}
	void call() {
		std::string number = this->getNumber();
		if (number == " ") return; 

		std::cout << "������������ �����: " << number << std::endl;
	}

	void sms() {
		std::string number = this->getNumber();
		
		if (number == " ") return;
		std::cout << "������������ ��� �� �����: " << number << std::endl;
	}

	void exit() {
		std::cout << "��������� �������" << std::endl;
	}
};


void task2() {
	int cmd = 0;
	Phone myPhone;

	//std::cout << "������ �2 ��������" << std::endl;
	while (1) {
		std::cout << "������� ����� ��������:\n1 - add\n2 - call\n3 - sms\n4 - exit" << std::endl;
		std::cin >> cmd;
		switch (cmd)
		{
		case 1:
			myPhone.addNumber();
			break;
		case 2:
			myPhone.call();
			break;
		case 3:
			myPhone.sms();
			break;
		case 4:
			myPhone.exit();
			return;
		default:
			break;
		}
	}
}